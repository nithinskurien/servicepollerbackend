package com.application.ServicePoller.controller;

import com.application.ServicePoller.model.ServiceEntity;
import com.application.ServicePoller.service.ServiceEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/* ServiceEntityController /******************************************************************
 * Controller file which creates the required API endpoints for GET, POST, PUT and DELETE.
 * Acts as an abstraction layer which can then forward incoming API requests to the logic layer.
 /****************************************************************************************/
@RestController
@RequestMapping("/api/v1/service")
@CrossOrigin(origins = "*")
public class ServiceEntityController {

    private final ServiceEntityService serviceEntityService;

    /**
     * Constructor that injects the serviceEntityService to handle the business logic
     * @param serviceEntityService The service that handles the api requests
     */
    @Autowired
    public ServiceEntityController(ServiceEntityService serviceEntityService) {

        this.serviceEntityService = serviceEntityService;
    }

    /**
     * Returns all the saved services and their attributes
     * API endpoint: /api/v1/service
     * @return ResponseEntity Response of the request to see all the services
     */
    @GetMapping
    public ResponseEntity<Object> getServices() {
        return serviceEntityService.getServices();
    }

    /**
     * Returns the attributes for a selected service
     * API endpoint for service e.g. test: /api/v1/service/test
     * @param serviceName name of service derived automatically form the path of the API endpoint e.g. test
     * @return ResponseEntity Response of the request to see selected service e.g. test
     */
    @GetMapping("/{serviceName}")
    public ResponseEntity<Object> getServiceByName(@PathVariable("serviceName") String serviceName){
        return serviceEntityService.getServiceByName(serviceName);
    }

    /**
     * Registers a new service to the backend
     * API endpoint for service e.g. test: /api/v1/service/register
     * @param service the service entity is passed via the body of the API call which gets transformed into ServiceEntity
     * @return ResponseEntity Response of the request to register new service
     */
    @PostMapping("/register")
    public ResponseEntity<Object> registerService(@RequestBody ServiceEntity service) {
        return serviceEntityService.registerService(service);
    }

    /**
     * Updates a service already in the database
     * API endpoint for service e.g. test: /api/v1/service/update/test
     * @param serviceName name of service derived automatically form the path of the API endpoint e.g. test
     * @param modServiceEntity the new service entity is passed via the body of the API call which gets transformed into ServiceEntity
     * @return ResponseEntity Response of the request to update an existing service
     */
    @PutMapping("/update/{serviceName}")
    public ResponseEntity<Object> updateService(@PathVariable("serviceName") String serviceName,
                              @RequestBody ServiceEntity modServiceEntity) {
        return serviceEntityService.updateService(serviceName, modServiceEntity);
    }

    /**
     * Deletes an existing service from the database
     * API endpoint for service e.g. test: /api/v1/service/delete/test
     * @param serviceName name of service derived automatically form the path of the API endpoint e.g. test
     * @return ResponseEntity Response of the request to delete an existing service
     */
    @DeleteMapping(path = "/delete/{serviceName}")
    public ResponseEntity<Object> deleteService(@PathVariable("serviceName") String serviceName) {
        return serviceEntityService.deleteService(serviceName);
    }


}
