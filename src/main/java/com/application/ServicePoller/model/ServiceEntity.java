package com.application.ServicePoller.model;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

/* ServiceEntity /******************************************************************
 * The model which encapsulates the attributes of the services. Represents the smallest unit that
 * will be representing the data in the database.
 /****************************************************************************************/
@Entity
@Data
@Table(name = "serviceEntity")
public class ServiceEntity {
    @Id
    @Column(nullable = false)
    @SequenceGenerator(
            name = "service_sequence",
            sequenceName = "service_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "service_sequence"
    )
    private Long id;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private String url;
    private String status;
    @CreationTimestamp
    private LocalDateTime created;
    @UpdateTimestamp
    private LocalDateTime updated;

    /**
     * Constructor used for most of the database operations
     * @param name name of the service
     * @param url url path to reach the service
     */
    public ServiceEntity(String name, String url, String status) {
        this.name = name;
        this.url = url;
        this.status = status;
    }

    /**
     * Default constructor
     */
    public ServiceEntity() {
    }
}
