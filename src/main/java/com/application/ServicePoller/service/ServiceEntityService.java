package com.application.ServicePoller.service;

import com.application.ServicePoller.model.ServiceEntity;
import com.application.ServicePoller.repository.ServiceEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.net.*;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* ServiceEntityService /******************************************************************
 * The main layer which does most of the business logic, the functions here are called by
 * the ServiceEntityController. The service layer initiates the database commands via the
 * ServiceEntityRepository.
 /****************************************************************************************/
@Service
@EnableScheduling
public class ServiceEntityService {

    public enum SERVER_STATUS {
        UP,
        DOWN,
        INVALID
    }
    private static final String DUPLICATE_RECORD_NAME_ERROR_MSG = "Service with provided name already exists";
    private static final String RECORD_MISSING_ERROR_MSG = "Service with provided name does not exist";
    private static final String NULL_DATA_ERROR_MSG = "Cannot use empty or null data";
    private static final String INVALID_URL_ERROR_MSG = "Invalid URL entered";
    private static final int NO_CODE = 0;
    private static final int SCHEDULED_TASK_INTERVAL = 30000; //Time in ms to run the scheduled job
    private final ServiceEntityRepository serviceEntityRepository;

    /**
     * Constructor which injects the ServiceEntityRepository
     * @param serviceEntityRepository JPA repository which can query the database
     */
    @Autowired
    public ServiceEntityService(ServiceEntityRepository serviceEntityRepository) {
        this.serviceEntityRepository = serviceEntityRepository;
    }

    /**
     * Returns all the saved services and their attributes
     * @return Response of the request to see all the services
     */
    public ResponseEntity<Object> getServices() {
        return ResponseEntity.status(HttpStatus.OK).body(serviceEntityRepository.findAll());
    }

    /**
     * Returns the attributes for a selected service
     * @param serviceName name of service we need to query for
     * @return ResponseEntity Response of the request to see queried service
     */
    public ResponseEntity<Object> getServiceByName(String serviceName) {
        Optional<ServiceEntity> serviceEntity = serviceEntityRepository.findByServiceEntityName(serviceName);
        if (serviceEntity.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(RECORD_MISSING_ERROR_MSG);
        }
        return ResponseEntity.status(HttpStatus.OK).body(serviceEntity);
    }

    /**
     * Registers a new service to the database
     * @param serviceEntity the ServiceEntity that needs to be added as a roe to the database
     * @return ResponseEntity Response of the request to register new service
     */
    public ResponseEntity<Object> registerService(ServiceEntity serviceEntity) {

        String serviceName = serviceEntity.getName();
        String serviceUrl = serviceEntity.getUrl();
        if (isNullOrEmpty(serviceName) || isNullOrEmpty(serviceUrl)) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(NULL_DATA_ERROR_MSG);
        }
        if (serviceEntityRepository.findByServiceEntityName(serviceName).isPresent()) {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(DUPLICATE_RECORD_NAME_ERROR_MSG);
        }
        if (!isValidUrl(serviceUrl)) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(INVALID_URL_ERROR_MSG);
        }

        serviceEntity.setStatus(getServiceStatus(serviceEntity.getUrl()).toString());
        serviceEntityRepository.save(serviceEntity);
        return ResponseEntity.status(HttpStatus.CREATED).body(serviceEntity);
    }

    /**
     * Updates a service already in the database. Uses Transactional annotation, hence we do not need explicit call
     * to write to database but change the entity itself
     * @param serviceName name of service to be updated
     * @param modServiceEntity ServiceEntity that needs to update the previous service
     * @return ResponseEntity Response of the request to update an existing service
     */
    @Transactional
    public ResponseEntity<Object> updateService(String serviceName, ServiceEntity modServiceEntity) {
        Optional<ServiceEntity> serviceEntity = serviceEntityRepository.findByServiceEntityName(serviceName);
        if (isNullOrEmpty(modServiceEntity.getUrl()) || isNullOrEmpty(modServiceEntity.getName())) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(NULL_DATA_ERROR_MSG);
        }
        if (!isValidUrl(modServiceEntity.getUrl())) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(INVALID_URL_ERROR_MSG);
        }
        if (serviceEntityRepository.findByServiceEntityName(modServiceEntity.getName()).isPresent()) {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(DUPLICATE_RECORD_NAME_ERROR_MSG);
        }
        if (serviceEntity.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(RECORD_MISSING_ERROR_MSG);
        }
        serviceEntity.map(service -> {
                    service.setName(modServiceEntity.getName());
                    service.setUrl(modServiceEntity.getUrl());
                    service.setStatus(getServiceStatus(modServiceEntity.getUrl()).toString());
                    return true;
                }
        );
        return ResponseEntity.status(HttpStatus.OK).body(serviceEntity);
    }


    /**
     * Deletes an existing service from the database
     * @param serviceName name of the service that needs to be deleted
     * @return ResponseEntity Response of the request to delete an existing service
     */
    public ResponseEntity<Object> deleteService(String serviceName) {
        Optional<ServiceEntity> serviceEntity = serviceEntityRepository.findByServiceEntityName(serviceName);
        if (serviceEntity.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(RECORD_MISSING_ERROR_MSG);
        }
        serviceEntityRepository.deleteById(serviceEntity.get().getId());
        return ResponseEntity.status(HttpStatus.OK).body(serviceEntity);
    }

    /**
     * A scheduled task that runs at a fixed interval and checks the status of all the services. The status is changed
     * in the database entry for that service if there is any change in it. As a transactional function it does not need
     * explicit database update commands, simple change in ServiceEntity is sufficient
     */
    @Scheduled (fixedRate = SCHEDULED_TASK_INTERVAL )
    @Transactional
    public void serverStatusUpdate() {
        serviceEntityRepository.findAll().forEach( serviceEntity -> {
            serviceEntity.setStatus(getServiceStatus(serviceEntity.getUrl()).toString());
        });
    }

    /**
     * Returns the server status by establishing connection, sending a request and seeing the response
     * @param url the url path for which the status needs to be checked
     * @return enum which shows the server status
     */
    public SERVER_STATUS getServiceStatus(String url) {
        int statusCode = NO_CODE;
        try {
            URL serviceURL = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) serviceURL.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();
            statusCode = connection.getResponseCode();
        }
        catch (ConnectException e) {
            return SERVER_STATUS.INVALID;
        } catch (IOException e) {
            e.printStackTrace();
            return SERVER_STATUS.INVALID;
        }

        return (statusCode == HttpStatus.OK.value()) ? SERVER_STATUS.UP : SERVER_STATUS.DOWN;
    }

    /**
     * Checks if a given URL is valid
     * @param url the URL that needs to be checked
     * @return if the URL is valid
     */
    private boolean isValidUrl(String url) {
        String regex = "^(http|https):[/]{2}[^ ]+$";
        Pattern pattern = Pattern.compile(regex);
        Matcher match = pattern.matcher(url);
        return match.matches();
    }

    /**
     * Checks if a particular string value is either null or empty
     * @param string the string that needs to be checked
     * @return if the string is either null or empty
     */
    private boolean isNullOrEmpty(String string) {
        return string == null || string.trim().isEmpty();
    }
}
