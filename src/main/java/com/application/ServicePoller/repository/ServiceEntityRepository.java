package com.application.ServicePoller.repository;

import com.application.ServicePoller.model.ServiceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

/* ServiceEntityRepository /******************************************************************
 * The JPA Repository which serves as the abstraction layer to doing operations on the Database
 /****************************************************************************************/
public interface ServiceEntityRepository extends JpaRepository<ServiceEntity, Long> {

    /**
     * Returns the ServiceEntity from the database with the requested name
     * @param name name of the service which we want to query from database
     * @return ServiceEntity which has the desired service name
     */
    @Query("SELECT service FROM ServiceEntity service WHERE service.name=?1")
    Optional<ServiceEntity> findByServiceEntityName(String name);
}
