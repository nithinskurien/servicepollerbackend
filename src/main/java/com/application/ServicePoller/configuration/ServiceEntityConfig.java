package com.application.ServicePoller.configuration;

import com.application.ServicePoller.model.ServiceEntity;
import com.application.ServicePoller.repository.ServiceEntityRepository;
import com.application.ServicePoller.service.ServiceEntityService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.List;

/* ServiceEntityConfig /******************************************************************
 * Configuration class used to create default data for populating the database with valid data.
 * Not to be used in production, the annotation @Configuration can be commented off to disable
 * the populating of the database.
 ****************************************************************************************/

@Configuration
public class ServiceEntityConfig {

    @Bean
    CommandLineRunner commandLineRunner(ServiceEntityRepository serviceEntityRepository, ServiceEntityService serviceEntityService){
        return  args -> {
            ServiceEntity wikipedia = new ServiceEntity(
                    "wiki",
                    "https://www.wikipedia.org/",
                    serviceEntityService.getServiceStatus("https://www.wikipedia.org/").toString()

            );
            ServiceEntity personalService = new ServiceEntity(
                    "service",
                    "http://localhost:8080/api/v1/service",
                    serviceEntityService.getServiceStatus("http://localhost:8080/api/v1/service").toString()
            );
            ServiceEntity personalSite = new ServiceEntity(
                    "site",
                    "http://localhost:8000/",
                    serviceEntityService.getServiceStatus("http://localhost:8000/").toString()
            );

            serviceEntityRepository.saveAll(List.of(wikipedia,personalService,personalSite));
        };
    }
}
