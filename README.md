# Service Poller Backend #

The Spring Boot based backend uses java to create a basic service poller. The API endpoints created by the backend are:

![Service Poller Read](./img/read.png)
![Service Poller Save](./img/save.png)

* GET: http://localhost:8080/api/v1/service/
* GET: http://localhost:8080/api/v1/service/servicename
* DELETE: http://localhost:8080/api/v1/service/delete/
* POST: http://localhost:8080/api/v1/service/register/
* PUT: http://localhost:8080/api/v1/service/update/

## Components ##

* Configurations - Involved in populating the database with some default values and CORS filter, to enable API calls from the frontend
* Controller Layer - It is responsible for creating the API endpoints, the request is routed to the service layer
* Service Layer - Takes care of the business logic and data validation. Also has a scheduled function that checks the status of the service every fixed interval
* Entity Model - Representation of the data stored to the table
* Repository - A MySQL database to persist the data


## Instructions ##

First a MySQL database needs to be created using the query, the MySQL Workbench can be useful for that:

`create database serviceEntity;`

The database should be created, if the default settings have not been changed the MySQL port should be on 3306 (can be checked by looking at the system variables of MySQL). If not then the application.properties file needs to be changed to reflect the port on which MySQL is running.

Location of File:
`src/main/resources`

the line to be modified is

`spring.datasource.url=jdbc:mysql://localhost:3306/serviceEntity?useSSL=false`

When opening the project the maven dependencies should start downloading automatically, if not use maven to reload the project using the pom.xml file. The application can be started by running the function ServicePollerApplication in :

`src/main/java/ServicePollerApplication.java`

This should fire up the backend and setup the tables in the database we previously created.

![Tables](./img/table.png)

Now using tools like POSTMAN we can see if all the APIs should be working fine!
Currently in the settings the application is set to create and drop tables when it boots up and closes this can be changed in the application.properties to make the tables persistent after restarts.

`spring.jpa.hibernate.ddl-auto=create-drop` 

can be changed to 

`spring.jpa.hibernate.ddl-auto=update` 

or can be commented off. Also the ServiceEntityConfig is responsible for populating the tables with default values as we drop the values from database every time we restart the app. If we want to change the ddl-auto setting to have persistent values even after app restart we should comment off the @Configuration annotation from the ServiceEntityConfig class to prevent it from adding data every time we restart the application.

## Frontend ##

![Service Poller Frontend](./img/update.png)

There is a submodule which has the implementation to a React frontend to interact with the API [Service Poller Frontend](https://bitbucket.org/nithinskurien/servicepollerfrontend/src/master/) the way to set it up and use is mentioned in the repo.